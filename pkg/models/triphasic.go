// Copyright © 2017 Calliope Gardner <calliope@chronojam.co.uk>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package models

import (
	"time"
)

// Triphasic sleep pattern is 3, 1.5 hour naps every 24 hours.
// We should wake up just before dawn, then every 6.5 hours afterwards.
type Triphasic struct {}

// Takes a sunrise and sunset time, then calculates sleep periods
func (t *Triphasic) Fit(rise, set time.Time) []SleepPeriod {
	daylightLength := set.Sub(rise)
	if daylightLength.Hours() < 12 {
		return []SleepPeriod{
			SleepPeriod{
				Start: 	set.Add(-time.Hour * 8 + time.Minute * 30),
				End: set.Add(time.Minute * 30 - time.Minute * 390 ),
			},
			SleepPeriod{
				// Goto sleep 30 minutes after the sun sets
				Start: set.Add(time.Minute * 30),
				End: set.Add(time.Hour * 2),
			},
			SleepPeriod{
				// start 6.5 hours after waking up from second nap
				Start: set.Add(time.Minute * 510),
				// end 8 hours after waking up from second nap.
				End: set.Add(time.Hour * 10),
			},
		}
	}
	return []SleepPeriod{
		SleepPeriod{
			// Go to sleep 60 minutes before the sun starts to rise
			Start: rise.Add(-time.Hour),
			// Wake up 30 minutes after the sun rises.
			End: rise.Add(time.Minute * 30),
		},
		SleepPeriod{
			// start 6.5 hours after waking up from first nap
			Start: rise.Add(time.Minute * 30 + time.Minute * 390),
			// end 8 hours after waking up from first nap
			End: rise.Add(time.Minute * 30 + time.Hour * 8),
		},
		SleepPeriod{
			Start: rise.Add(time.Hour * 15),
			End: rise.Add(time.Hour * 16 + time.Minute * 30),
		},
	}
}