// Copyright © 2017 Calliope Gardner <calliope@chronojam.co.uk>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package models

import (
	"testing"
	"time"
	"github.com/stretchr/testify/assert"
)

func TestTriphasicWinter(t *testing.T) {
	tri := &Triphasic{}
	r := tri.Fit(
		time.Date(2022, 2, 3, 7, 22, 30, 0, time.UTC),
		time.Date(2022, 2, 3, 16, 44, 22, 0, time.UTC),
	)
	for _, p := range r {
		assert.Equal(t, int(p.End.Sub(p.Start).Minutes()), 90)
	}
}

func TestTriphasicSummer(t *testing.T) {
	tri := &Triphasic{}
	r := tri.Fit(
		time.Date(2022, 8, 3, 5, 35, 43, 0, time.UTC),
		time.Date(2022, 8, 3, 20, 57, 28, 0, time.UTC),
	)
	for _, p := range r {
		assert.Equal(t, int(p.End.Sub(p.Start).Minutes()), 90)
	}
}

func TestTriphasicSequentialDays(t *testing.T) {
	tri := &Triphasic{}
	first := tri.Fit(
		time.Date(2022, 8, 3, 5, 35, 43, 0, time.UTC),
		time.Date(2022, 8, 3, 20, 57, 28, 0, time.UTC),
	)
	second := tri.Fit(
		time.Date(2022, 8, 4, 5, 37, 15, 0, time.UTC),
		time.Date(2022, 8, 4, 20, 55, 45, 0, time.UTC),
	)
	all := append(first, second...)
	previousEnd := all[0].End.Add(time.Hour * -8)
	for _, p := range all {
		assert.Equal(t, 8, int(p.End.Sub(previousEnd).Hours()))
		previousEnd = p.End
	}
}