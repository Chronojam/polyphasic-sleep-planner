// Copyright © 2017 Calliope Gardner <calliope@chronojam.co.uk>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

/*
	Credit is given to https://sunrise-sunset.org/api
*/
package sunrise

import (
	"time"
	"net/http"
	"fmt"

	"encoding/json"
	"io/ioutil"
	"errors"
)

type SunriseResponseResults struct {
	Sunrise time.Time `json:"sunrise"`
	Sunset	time.Time `json:"sunset"`
	SolarNoon time.Time `json:"solar_noon"`
	DayLength int `json:"day_length"` // Day length in seconds.
	
	XXX map[string]string `json:"-"` // Other fields.
}
type SunriseResponse struct {
	Results *SunriseResponseResults `json:"results"`
	Status string `json:"status"`
}

func SSForTime(lat, long float64, instant time.Time) (*SunriseResponseResults, error) {
	date := fmt.Sprintf("%v-%d-%v", instant.Year(), instant.Month(), instant.Day())
	url := fmt.Sprintf("https://api.sunrise-sunset.org/json?lat=%v&lng=%v&date=%v&formatted=0", lat, long, date)
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var typedResponse SunriseResponse
	err = json.Unmarshal(b, &typedResponse)
	if err != nil {
		return nil, err
	}

	if typedResponse.Status != "OK" {
		return nil, errors.New("Status Not OK!")
	}

	return typedResponse.Results, nil
}
