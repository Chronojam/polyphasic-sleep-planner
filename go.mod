module gitlab.com/chronojam/polyphasic-sleep-planner/v2

go 1.17

require (
	github.com/arran4/golang-ical v0.0.0-20220115055431-e3ae8290e7b8 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
