# Polyphasic Sleep Planner

This tool adapts polyphasic sleep patterns to function with shifting daylight times the idea being that its easier on your circadian rhythm.

## Installation

You can use directly from this repo using bazel
```
bazel run //cmd -- {args}
```

Or you can use one of the prebuilt binaries from the releases
```
./linux_amd64 {args}
./darwin_amd64 {args}
```

## Usage

```
# Output the next 5 days worth of naps into a json file
# using Bristol (UK) as the location
./sleep-planner -o json -output-file ./results.json

# Output the next 10 days worth of naps into an ical file
# using NYC as the location
./sleep-planner -o ical -output-file ./naps.ical -latitude 40.7128 -longitude 74.0060 -days 10

Args:
  -days int
        Number of days ahead to calculate (default 5)
  -latitude float
        Latitude Coordinates (default 51.4545)
  -longitude float
        Longitude Coordinates (default 2.5879)
  -o string
        Output format, "json" or "ical" (default "json")
  -output-file string
        path to output file. (default "./results.json")
```

## Building

This repo uses bazel (tested version 5.0.0) to build.

```
bazel build //cmd

# All builds and tests
bazel build //...
bazel test //...
```