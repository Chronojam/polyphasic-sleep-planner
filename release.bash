#/usr/bin/env bash

for platform in "linux_amd64" "darwin_amd64"; do
    bazel build --platforms=@io_bazel_rules_go//go/toolchain:$platform //cmd
    curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
     --upload-file bazel-bin/cmd/cmd_/cmd \
     "https://gitlab.com/api/v4/projects/Chronojam%2Fpolyphasic-sleep-planner/packages/generic/binaries/1.0.0/$platform?status=default"
done
