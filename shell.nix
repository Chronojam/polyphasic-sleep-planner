{ pkgs ? import <nixpkgs> {} }:

(pkgs.buildFHSUserEnv {
    name = "bazel-polyphasic-sleep-planner-environment";
    targetPkgs = pkgs: [
        pkgs.bazel_4
        pkgs.glibc
        pkgs.gcc
    ];
}).env