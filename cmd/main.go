// Copyright © 2017 Calliope Gardner <calliope@chronojam.co.uk>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


package main

import (
	"flag"
	"fmt"
	"log"
	"time"
	"encoding/json"
	"io/ioutil"

	"github.com/arran4/golang-ical"
	
	"gitlab.com/chronojam/polyphasic-sleep-planner/pkg/sunrise"
	"gitlab.com/chronojam/polyphasic-sleep-planner/pkg/models"
)

var (
	latitude = flag.Float64("latitude", 51.4545, "Latitude Coordinates")
	longitude = flag.Float64("longitude", 2.5879, "Longitude Coordinates")
	days = flag.Int("days", 5, "Number of days ahead to calculate")
	output = flag.String("o", "json", "Output format, \"json\" or \"ical\"")
	outputFile = flag.String("output-file", "./results.json", "path to output file.")
)

func main() {
	flag.Parse()
	now := time.Now()
	m := models.Triphasic{}
	periods := []models.SleepPeriod{}
	for x := 0; x < *days; x++ {
		hours := time.Duration(x * 24) * time.Hour
		r, err := sunrise.SSForTime(*latitude, *longitude, now.Add(hours))
		if err != nil {
			log.Fatalf("%v", err)
		}
		periods = append(periods, m.Fit(r.Sunrise, r.Sunset)...)
	}

	switch *output {
	case "json":
		b, err := json.MarshalIndent(periods, "", "\t")
		if err != nil {
			log.Fatalf("%v", err)
		}
		err = ioutil.WriteFile(*outputFile, b, 0777)
		if err != nil {
			log.Fatalf("%v", err)
		}
		break
	case "ical":
		cal := ics.NewCalendar()
		cal.SetMethod(ics.MethodRequest)
		for i, p := range periods {
			event := cal.AddEvent(fmt.Sprintf("%d", i))
			event.SetCreatedTime(time.Now())
			event.SetDtStampTime(time.Now())
			event.SetModifiedAt(time.Now())
			event.SetStartAt(p.Start)
			event.SetEndAt(p.End)
			event.SetSummary("Nap")
		}
		err := ioutil.WriteFile(*outputFile, []byte(cal.Serialize()), 0777)
		if err != nil {
			log.Fatalf("%v", err)
		}
		break
	}
}